﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace SmallApplication
{

    class Program
    {

    
        static int SumaLiczbPodzielnycPrzezTrzy(int[] tablica)
        {

            int suma = 0;

            for (int i = 0; i < tablica.Length; i++)
            {
                if (tablica[i] % 3 == 0)
                    suma = suma + tablica[i];
            }

            return suma;
        }


        static void Main(string[] args)
        {
            int[] tablica1 = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };

            int wynik = Program.SumaLiczbPodzielnycPrzezTrzy(tablica1);

            Console.WriteLine(wynik);

            Console.ReadKey();


        }


    }

}
